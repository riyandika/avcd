<?php
	class stegano{
		function isGenap($cek){
			//buat ngecek genap apa engga
			return ($cek%2==0);
		}
		
		function asc2bin($char){
			//ngubah ascii jadi biner 8bit
			return str_pad(decbin(ord($char)), 8, "0", STR_PAD_LEFT);
		}
		
		function bin2asc($bin){
			//ngubah biner jadi ascii
			return chr(bindec($bin));
		}
		
		function rgb2bin($rgb){
			//ngehasilin deretan biner di satu pixel
			$biner = "";
			$red = ($rgb >> 16) & 0xFF;
			$green = ($rgb >> 8) & 0xFF;
			$blue = $rgb & 0xFF;
			
			if($this->isGenap($red)){
				$biner .= "0";
			}else{
				$biner .= "1";
			}
			if($this->isGenap($green)){
				$biner .= "0";
			}else{
				$biner .= "1";
			}
			if($this->isGenap($blue)){
				$biner .= "0";
			}else{
				$biner .= "1";
			}
			
			return $biner;
		}
	
		function img2string($pic,$size){
			// converting image to string
			// size = array (0=x 1=y)
			// zero last bit before string : enolin bit terakhir
			
			$stream = "";
			
			$y=0;
			for($x=0; $y<$size[1] ;$x++){
				
				// baca RGB dari pixel
				
				$rgb = ImageColorAt($pic, $x, $y);
				
				$cols = array();
				$cols[] = ($rgb >> 16) & 0xFF;	//red
				$cols[] = ($rgb >> 8) & 0xFF;	//green
				$cols[] = $rgb & 0xFF;			//blue
				
				for($j=0; $j<sizeof($cols); $j++){
					if(!$this->isGenap($cols[$j])){
						$cols[$j]--;			//kalo engga genap ya digenapin
					}
					$stream .= chr($cols[$j]);
				}
				
				// if at end of X, move down and start at x=0
				if($x==($size[0]-1)){
					$y++;
					$x=-1;
				}
			}
			return $stream;
		}
		
		function stegIn($pic,$attributes,$outpic,$teks){
			$make_odd = array();
			
			for($i=0;$i<strlen($teks);$i++){
				//char jadi 8bit
				$char = $teks[$i];
				$biner = $this -> asc2bin($char);
				
				//bikin array true/false
				for($j=0;$j<strlen($biner);$j++){
					$binpart = $biner[$j];
					if($binpart=="0"){
						$make_odd[] = false;
					}else{
						$make_odd[] = true;
					}
				}
			}
			
			//modifikasi nilai rgb berdasar make_odd nya
			
			$y = 0;
			for($i=0,$x=0; $i<sizeof($make_odd);$i+=3,$x++){
				//baca rgb piksel
				$rgb = imagecolorat($pic,$x,$y);
				$cols = array();
				$cols[] = ($rgb >> 16) &0xFF;
				$cols[] = ($rgb >> 8) &0xFF;
				$cols[] = $rgb &0xFF;
				
				for($j=0;$j<sizeof($cols);$j++){
					if($make_odd[$i+$j]==true && $this -> isGenap($cols[$j])){
						$cols[$j]++;
					}elseif($make_odd[$i+$j]==false && !$this -> isGenap($cols[$j])){
						$cols[$j]--;
					}
				}
				
				//modifikasi piksel
				$temp_col = imagecolorallocate($outpic,$cols[0],$cols[1],$cols[2]);
				imagesetpixel($outpic,$x,$y,$temp_col);
				
				//kalo abis sebaris, buat ganti ke baris selanjutnya
				if($x==($attributes[0]-1)){
					$y++;
					$x=-1;
				}
			}
			
			//output jadi PNG soalnya ga terkompresi
			header("Content-type: image/png");
			header("Content-Disposition: attachment; filename=Dokumen.png");
			imagePNG($outpic);
			
			//hapus gambar
			imagedestroy($outpic);
			imagedestroy($pic);
			exit();
		}
	}
?>