<?php
	
	class kripto{
		function oneTimePad($teks,$kunci,$r=null,$iterasi=null){
			/*
			one time pad bisa buat enkripsi dan dekripsi
			kalo panjang teks cuma satu karakter berarti cuman enkripsi per karakter.
			kalo lebih dari satu karakter, maka kunci selanjutnya digenerate di sini.
			*/
			
			$panjang = strlen($teks);
			if($panjang>1){
				$hasil = "";
				if($r!=null || $iterasi!=null){
					$hasil = "";
					for($i=0;$i<$panjang;$i++){
						$k = $this -> pembulatan($kunci,256);
						$hasil .= chr(ord($teks[$i])^$k);
						$kunci = $this -> chaos($kunci,$r,$iterasi);
					}
				}
			}else{ //kalo cuman satu karakter
				$k = $this -> pembulatan($kunci,256);
				$hasil = chr(ord($teks)^$k);
			}
			return $hasil;
		}
		
		function chaos($awal, $r, $iterasi){
			
			$x = $awal;
			for($i=1;$i<=$iterasi;$i++){
				$x = $r*$x*(1-$x);
			}
			
			return $x;
		}
		
		function hash2float($hashCitra,$hashKunci){
			$potong1 = str_split($hashCitra,7);
			$potong2 = str_split($hashKunci,7);
			$hasil = 0;
			
			for ($i=0;$i<count($potong1);$i++){
				$irisan = hexdec($potong1[$i]) ^ hexdec($potong2[$i]);
				if ($i%3 == 0){
					$temp = $irisan;
					if (($i+1) == count(potong1)){
						$hasil = $hasil ^ $temp;
					}
				}elseif ($i%3 == 1){
					$temp = $temp + $irisan;
					if (($i+1) == count(potong1)){
						$hasil = $hasil ^ $temp;
					}
				}else{
					$hasil = $hasil ^ $temp;
				}
			}
			$hasil = $hasil / pow(10,9);
			return $hasil;
		}
		
		function pembulatan($input, $limit){
			return ((pow(10, strlen($limit)) * $input) % $limit);
		}
	}
?>