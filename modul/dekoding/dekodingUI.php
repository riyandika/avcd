<?php
	if (isset($_FILES['gambar'])){
		$tampil = new dekodingUI();
		$tampil -> show();
	}
	echo '
		<div id="petunjuk" title="Petunjuk">
			Selamat datang di Aplikasi Validasi Citra Dokumen<br>
			Silakan masukkan file gambar yang akan diperiksa keasliannya, kemudian tekan tombol \'periksa\'<br>
			Citra yang dimasukkan harus berekstensi/format .png<br>
			<img src="'.ALAMAT.'/petunjuk/dekoding1.jpg" width="280px">
		</div>
	';
	class dekodingUI{
		function show(){
			require 'dekoding.php';
			//require 'modul/pengaturan/pengaturan.php';
			require 'modul/kripto/kripto.php';
			require 'modul/stegano/stegano.php';
			$dekode = new dekoding();
			$hasil = $dekode -> dekode($_FILES['gambar']);
			$asli = $hasil['asli'];
			$teks = $hasil['teks'];
			if(isset($asli) && $asli===true){
				echo '
				<h1 class="title">Dokumen '.$_FILES['gambar']['name'].' <b>ASLI</b></h1>
				<div class="entry">';
				if($hasil['teks']){
					echo '
					Teks yang terkandung dalam dokumen :<br>
					'.$hasil['teks'];
				}
				echo '
				</div>
				';
			}elseif($asli != true){
				echo '
				<h1 class="title">Dokumen '.$_FILES['gambar']['name'].' <b>TIDAK</b> Teridentifikasi Asli</h1>
				<div class="entry">
				Silakan hubungi instansi terkait untuk mendapatkan dokumen yang teridentifikasi asli.
				</div>
				';
			}else{
				echo '
				<h1 class="title">Terjadi Kesalahan</h1>
				<div class="entry">
				'.$hasil.'
				</div>
				';
			}
		}
	}
?>