<?php
	
	class dekoding{
		function dekode($citra){
			if ($citra['type']=='image/png'){ //cek png
				$pengaturan = new pengaturan();
				$kripto = new kripto();
				$rs = $pengaturan ->getPengaturan();
				$param = mysql_fetch_array($rs);
				$stegano = new stegano();
				
				$boundary = "";
				$binary = "";
				$hash = "";
				$plain = "";
				$n_boundary = 1; //jumlah boundary mulai dari 1
				
				//create citra
				$pic = @ImageCreateFromPNG($citra['tmp_name']);
				$attributes = @getImageSize($citra['tmp_name']);
				
				if(!$pic || !$attributes){
					return 'gambarnya ga bisa dibuka, mungkin GDlib belum diinstal';
				}
				
				//ambil hash citra sekalian pembangkitan kunci awal
				$string_citra = $stegano -> img2string($pic,$attributes);
				$hash_citra = hash($param['algHash'],$string_citra);
				$hash_kunci = hash($param['algHash'],$param['kunci']);
				$kunci = $kripto -> hash2float($hash_citra,$hash_kunci);
				$kunci = $kripto -> chaos($kunci,$param['nilaiR'],$param['jmlIterasi']);
				
				//ambil boundary
				$biner_boundary = "";
				for($x=0; $x<8;$x++){
					$biner_boundary .= $stegano -> rgb2bin(ImageColorAt($pic,$x,0));
				}
				
				//bikin boundary
				for($i=0;$i<strlen($biner_boundary);$i+=8){
					$temp = substr($biner_boundary,$i,8);
					$temp_boundary = $stegano -> bin2asc($temp);
					$boundary .= $kripto -> oneTimePad($temp_boundary,$kunci);
					
					//update kunci
					$kunci = $kripto -> chaos($kunci,$param['nilaiR'],$param['jmlIterasi']);
				}
				
				//konversi RGB tiap piksel ke biner, berhenti kalo liat boundary lagi
				
				//boundary ga diproses
				$asli = true;
				$start_x = 8;
				$i = 0;
				for($y=0; $y<$attributes[1];$y++){
					for($x=$start_x;$x<$attributes[0];$x++){
						
						// generate binary
						$binary .= $stegano -> rgb2bin(ImageColorAt($pic,$x,$y));
						
						//ubah ke ascii
						if(strlen($binary)>=8){
							$temp = substr($binary,0,8);
							$temp_chip = $stegano -> bin2asc($temp);
							$binary = substr($binary,8);
							$temp_plain = $kripto -> oneTimePad($temp_chip,$kunci);
							
							//update kunci
							$kunci = $kripto -> chaos($kunci,$param['nilaiR'],$param['jmlIterasi']);
							
							//masuk ke hash ato plain
							if($n_boundary==1){
								if($i<strlen($hash_citra)){
									if($temp_plain==$hash_citra[$i]){
										$i++;
									}else{
										$asli = false;
										break 2;
									}
								}
								$hash .= $temp_plain;
								
								//kalo nemu boundary kedua
								if(strpos($hash,$boundary)!=false){
									//hapus boundary
									$hash = substr($hash,0,strlen($hash)-3);
									$n_boundary++;
								}
							}elseif($n_boundary==2){
								$plain .= $temp_plain;
								
								//kalo nemu boundary ketiga
								if(strpos($plain,$boundary)!=false){
									//hapus boundary
									$plain = substr($plain,0,strlen($plain)-3);
									$n_boundary++;
									
									//keluar iterasi
									break 2;
								}
							}
						}
					}
					
					$start_x = 0;
				}
				
				//$asli = $hash_citra == $hash;
				$hasil = array('asli' => $asli, 'teks' => $plain);
				return $hasil;
			}elseif($citra['error']!==4){
				return 'cuman bisa png';
			}else{
				return 'mohon masukkan citra yang akan diperiksa';
			}
		}
	}
?>