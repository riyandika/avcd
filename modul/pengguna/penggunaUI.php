<?php
	require 'penggunaCtrl.php';
	require 'pengguna.php';
	$tampil = new penggunaUI();
	if(isset($r)){
		$tampil -> ubah($r);
	}else{
		if(isset($_POST['tambah'])){
			$ctrl = new penggunaCtrl();
			$hasil = $ctrl -> tambahPengguna($_POST);
		}elseif(isset($_POST['ubah'])){
			$ctrl = new penggunaCtrl();
			$hasil = $ctrl -> ubahPengguna($_POST);
		}
		$tampil -> tambah();
		$tampil -> showAll();
	}
	echo '
		<div id="petunjuk" title="Petunjuk">
			Pengaturan pengguna/pengurus aplikasi<br>
			Silakan masukkan nama pengguna dan kata kunci untuk menambahkan pengguna<br>
			klik <img src="'.ALAMAT.'/petunjuk/b_edit.png"> untuk mengubah nama pengguna dan kata sandi<br>
			klik <img src="'.ALAMAT.'/petunjuk/b_drop.png"> untuk menghapus pengguna<br>
			<img src="'.ALAMAT.'/petunjuk/pengguna1.jpg" width="280px">
		</div>
	';
	
	class penggunaUI{
		function ubah($nama){
			$ctrl = new penggunaCtrl();
			$rs = $ctrl -> tampilNama($nama);
			if(mysql_num_rows($rs)>0){
				$tn=mysql_fetch_array($rs);
?>
			<h1 class="title">Pengguna</h1>
			<div class="entry">
				<form method="post" action="<?php
					echo ALAMAT."/admin/pengguna.html";
				?>">
					<table>
						<tr>
							<th rowspan="2"> Ubah <br> Pengguna </th>
							<td>nama pengguna</td>
							<td>kata kunci</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><input type="text" name="nama" value="<?php
								echo $tn['nama'];
							?>"></td>
							<td><input type="password" name="pass"></td>
							<td><input type="submit" name="ubah" value="simpan"></td>
						</tr>
					</table>
					<input type="hidden" name="namaasli" value="<?php echo $nama; ?>">
				</form>
			</div>
<?php
			}else{
				echo "<script type=\"text/javascript\">window.location=\"".ALAMAT."/admin/pengguna.html\"</script>";
			}
		}
		
		function  tambah(){
?>
			<h1 class="title">Pengguna</h1>
			<div class="entry">
				<form method="post" action="">
					<table>
						<tr>
							<th rowspan="2"> Tambah <br> Pengguna </th>
							<td>nama pengguna</td>
							<td>kata kunci</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><input type="text" name="nama"></td>
							<td><input type="password" name="pass"></td>
							<td><input type="submit" name="tambah" value="simpan"></td>
						</tr>
					</table>
				</form>
			</div>
<?php
		}
		
		function showAll(){
?>
			<div class="entry">
			<br>
<?php
				$ctrl = new penggunaCtrl();
				$rs = $ctrl -> tampilSemua();
				if(mysql_num_rows($rs)>0){
					echo "<table>
		<tr>
			<td>nama pengguna</td>
			<td>&nbsp;</td>
		</tr>";
					while($tn=mysql_fetch_array($rs)){
						echo "<tr>
							<td>".$tn['nama']."</td>
							<td>";
						echo "<a href=\"".ALAMAT."/admin/pengguna.html?r=".$tn['nama']."\" title=\"ubah\"><img src=\"".ALAMAT."/images/b_edit.png\"></a> ";
						if ($tn['nama']!=$_SESSION['uname']){
							echo "<a href=\"".ALAMAT."/proses/pengguna.php?p=hapus&q=".$tn['nama']."\" onclick=\"return confirm('hapus ".$tn['nama']."')\" title=\"hapus\"><img src=\"".ALAMAT."/images/b_drop.png\"></a>";
						}
						echo "</td></tr>";
					}
				}
?>
				</table>
			</div>
<?php
		}
	}
?>