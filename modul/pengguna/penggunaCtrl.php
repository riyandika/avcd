<?php
	session_start();
		
	class penggunaCtrl{
		function ubahPengguna($input){
			$entitas = new pengguna();
			$hasil = $entitas -> ubahPengguna($input['nama'],$input['pass'],$input['namaasli']);
			if($hasil){
				$_SESSION['uname'] = $input['nama'];
			}
			return $hasil;
		}
		function tampilNama($nama){
			$entitas = new pengguna();
			$hasil = $entitas -> tampilNama($nama);
			return $hasil;
		}
		function tambahPengguna($input){
			$entitas = new pengguna();
			$hasil = $entitas -> tambahPengguna($input['nama'],$input['pass']);
			return $hasil;
		}
		
		function tampilSemua(){
			$entitas = new pengguna();
			$hasil = $entitas -> semua();
			return $hasil;
		}
		
		function hapus($input){
			$entitas = new pengguna();
			$hasil = $entitas -> hapus($input['q']);
			return $hasil;
		}
	}
?>