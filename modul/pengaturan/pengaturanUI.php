<?php
	$tampil = new pengaturanUI();
	$ctrl = new pengaturanCtrl();
	if($_POST['tipe']=='tambah'){
		$ctrl -> tambah($_POST);
	}elseif($_POST['tipe']=='ubah'){
		$ctrl -> ubah($_POST);
	}
	echo '
		<div id="petunjuk" title="Petunjuk">
			Pengaturan aplikasi validasi citra dokumen<br>
			<img src="'.ALAMAT.'/petunjuk/pengaturan1.jpg" width="280px"><br>
			<br>
			Pembangkit kunci acak berbasis algoritma Chaos dengan persamaan<br>
			<img src="'.ALAMAT.'/petunjuk/pengaturan2.jpg"><br>
			Nilai \'r\' menunjukkan tingkat perubahan angka acak, nilai \'r\' bernilai antara 0 dan 4.<br>
			Jumlah iterasi menentukan jumlah perulangan pembangkitan kunci, isi dengan bilangan bulat lebih dari 0.<br>
			Algoritma hash digunakan untuk mengambil pesan ringkas (message digest) dari citra.<br>
			Kunci aplikasi digunakan untuk membedakan aplikasi satu dengan lainnya.
		</div>
	';
	$tampil -> show();
	class pengaturanUI{
		function show(){
			$tampil = new pengaturanCtrl();
			$rs = $tampil -> ambilSatu();
			$tn = mysql_fetch_array($rs);
?>
			<h1 class="title">Pengaturan</h1>
			<div class="entry">
			<script type="text/javascript">
				function validasi(namaForm){
					nr=namaForm.nilair.value;
					ntipe=namaForm.tipe.value;
					if(nr>=0 && nr<=4){
						if(ntipe=='ubah'){
							return confirm("Apakah ingin menyimpan perubahan?");
						}else{
							return true;
						}
						
					}else{
						alert("nilai R harus antara 0-4");
						return false;
					}
				}
			</script>
				<form method="post" action="" onsubmit="return validasi(this)">
<?php
			if(mysql_num_rows($rs)==1){echo "<input type=\"hidden\" name=\"tipe\" value=\"ubah\">";}else{echo "<input name=\"tipe\" type=\"hidden\" value=\"tambah\">";}
?>
					<table>
						<tr>
							<td><label for="nilair">nilai R (0-4): </label></td>
							<td><input id="nilair" type="text" name="nilair" <?php if(isset($tn['nilaiR'])){echo "value=".$tn['nilaiR'];} ?>></td>
						</tr>
						<tr>
							<td><label for="jmlIterasi">jumlah iterasi : </label></td>
							<td><input id="jmlIterasi" type="text" name="jml" <?php if(isset($tn['jmlIterasi'])){echo "value=".$tn['jmlIterasi'];} ?>></td>
						</tr>
						<tr>
							<td><label for="algHash">algoritma hash : </label></td>
							<td><select id="algHash" name="algHash">
<?php
			foreach(hash_algos() as $has){
				echo "<option value=\"".$has."\"";
				if($tn['algHash']==$has){echo " selected";}
				echo ">".$has."</option>\n";
			}
?>
							</select></td>
						</tr>
						<tr>
							<td><label for="kunci">kunci : </label></td>
							<td><input id="kunci" type="text" name="kunci" <?php if(isset($tn['kunci'])){echo "value=".$tn['kunci'];} ?>></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="reset" value="reset"> <input type="submit" value="simpan"></td>
						</tr>
					</table>
				</form>
			</div>
<?php
		}
	}
?>