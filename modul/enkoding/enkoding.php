<?php
session_start();
	
	class enkoding{
		function enkode($citra, $teks_asal){
			if($citra['error']!==4){
				if ($citra['type']=='image/jpeg'){ //cek jpeg
					//print_r($citra);
					$teks = htmlentities($teks_asal);
					$teks = str_replace("'","&#039;",$teks);
					$pengaturan = new pengaturan();
					$kripto = new kripto();
					$rs = $pengaturan -> getPengaturan();
					$param = mysql_fetch_array($rs);
					$stegano = new stegano();
					
					//create citra
					$pic = @ImageCreateFromJPEG($citra['tmp_name']);
					$attributes = @getImageSize($citra['tmp_name']);
					$outpic = @ImageCreateFromJPEG($citra['tmp_name']);
					
					if(!$pic || !$attributes || !$outpic){
						//'gambarnya ga bisa dibuka, mungkin GDlib belum diinstal';
						return 1;
					}
					
					//bikin hash citra sekalian pembangkitan kunci awal
					$string_citra = $stegano -> img2string($pic,$attributes);
					$hash_citra = hash($param['algHash'],$string_citra);
					$hash_kunci = hash($param['algHash'],$param['kunci']);
					$kunci = $kripto -> hash2float($hash_citra,$hash_kunci);
					$kunci = $kripto -> chaos($kunci,$param['nilaiR'],$param['jmlIterasi']);
					
					//bikin boundary alias pembatas
					do{
						$boundary = chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255));
					} while(strpos($hash_citra,$boundary)!=false && strpos($teks,$boundary)!=false);
					
					//nambahin boundary ke hash ama teks
					$data = $boundary.$hash_citra.$boundary.$teks.$boundary;
					
					//cek gambarnya cukup dimasukin $data apa engga
					if(strlen($data)*8 > ($attributes[0]*$attributes[1]*3)){
						//hancurin gambarnya di memori
						imagedestroy($outpic);
						imagedestroy($pic);
						//'teksnya kepanjangan, gambarnya ga muat';
						return 2;
					}else{
						$chip_teks = $kripto -> oneTimePad($data,$kunci,$param['nilaiR'],$param['jmlIterasi']);
						$stegano -> stegIn($pic,$attributes,$outpic,$chip_teks);
					}
				}else{
					//'cuman bisa .jpg ya';
					return 3;
				}
			}else{
				return 4;
			}
		}
	}
?>