<?php
	$error = null;
	if (isset($_GET['err'])){$error = $_GET['err'];}
	$tampil = new enkodingUI();
	$tampil -> show($error);
	echo '
		<div id="petunjuk" title="Petunjuk">
			Proses pemberian tanda pada citra dokumen<br>
			Silakan masukkan file citra dokumen yang akan diberi tanda dan teks yang ingin disisipkan, kemudian tekan tombol \'proses\'<br>
			Citra yang dimasukkan harus berekstensi/format .jpg<br>
			<img src="'.ALAMAT.'/petunjuk/enkoding1.jpg" width="280px">
		</div>
	';
	
	class enkodingUI{
		function show($error = null){
			if($error == 1){
				echo '<script type="text/javascript">alert("gambarnya ga bisa dibuka, mungkin GDlib belum diinstal");</script>';
			}elseif($error == 2){
				echo '<script type="text/javascript">alert("teks terlalu panjang, pilih gambar dengan ukuran lebih besar");</script>';
			}elseif($error == 3){
				echo '<script type="text/javascript">alert("hanya bisa .jpg");</script>';
			}elseif($error ==4){
				echo '<script type="text/javascript">alert("masukkan gambar yang akan diperiksa");</script>';
			}
			echo '
			<h1 class="title">Enkoding</h1>
			<div class="entry">
			<form method="post" action="'.ALAMAT.'/proses/enkoding.php" enctype="multipart/form-data">
					<table>
						<tr>
							<td><label for="gambar">Gambar : </label></td>
							<td><input id="gambar" type="file" name="gambar" width="810" cols="50"></td>
						</tr>
						<tr>
							<td><label for="teks">Teks : </label></td>
							<td><textarea id="teks" name="teks" cols="50"></textarea></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="reset" value="hapus"> <input name="enkoding" type="submit" value="proses" onclick="this.value=\'processing...\';"></td>
						</tr>
					</table>
				</form>
			</div>';
		}
	}
?>