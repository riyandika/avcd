<?php
	session_start();
	
	require "koneksi.php";
	(isset($_GET['p']))? $p=$_GET['p'] : $p=null;
	(isset($_GET['q']))? $q=$_GET['q'] : $q=null;
	(isset($_GET['r']))? $r=$_GET['r'] : $r=null;
	
	if($q=='logout'){
		$_SESSION['uname'] = null;
		session_unregister('uname');
		session_destroy();
	}
	
	(isset($_SESSION['uname']))? $uname=$_SESSION['uname'] : $uname=null;
	
	require 'modul/pengaturan/pengaturanCtrl.php';
	$cek = new pengaturanCtrl();
	
	if($p==ADMIN_PAGE){
		if(isset($uname)){
			if($cek->adaAturan()){
				if($q=='enkoding'){
					$halaman = 'enkoding';
				}elseif($q=='pengguna'){
					$halaman = 'pengguna';
				}elseif($q=='pengaturan'){
					$halaman = 'pengaturan';
				}else{
					$halaman = 'muka';
				}
			}else{
				$halaman = 'pengaturan';
			}
		}else{
			$halaman = 'login';
		}
	}else{
		$halaman = 'dekoding';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>
<title>Periksa Dokumenmu :D</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="<?php echo ALAMAT.'/default1.css'; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo ALAMAT.'/images/title.png'; ?>" rel="shortcut icon" type="image/x-icon">
<script type="text/javascript" src="<?php echo ALAMAT.'/jquery-1.5.1.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo ALAMAT.'/jquery-ui-1.8.11.custom.min.js'; ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo ALAMAT.'/jquery/jquery.ui.all.css'; ?>" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#petunjuk").dialog({
			autoOpen	: false,
			resizable	: false,
			show		: "slide",
			hide		: "fade",
			width		: "350px",
			buttons: {
				"OK": function() {
				$( this ).dialog("close");
		    }
		 }
		});
		$("#tombol").click(function() {
			$("#petunjuk").dialog("open");
		})
	});
</script>
<style type="text/css">
   .ui-dialog-titlebar   { 
       color: #A5E543;
       background: black 
   }
   .ui-widget-content  { 
      background: white; 
   }
   
   .ui-dialog-content {
      font-size: 12px;
      font-family: Georgia, "Times New Roman", Times, serif;
   }
   .ui-button-text {
      color: #A5E543; 
      background-color: black;
   }
</style>
</head>
<body>
<div id="wrapper<?php if($p==ADMIN_PAGE){echo "a";} ?>">
<!-- start header -->
<div id="header">
	<div id="menu">
		<ul>
			<li <?php if($halaman=='dekoding'){echo' class="current_page_item"';} ?> ><a href="<?php echo ALAMAT; ?>">Dekoding</a></li>
<?php if (isset($uname)){ ?>
			<li <?php if($halaman=='enkoding'){echo' class="current_page_item"';} ?> ><a href="<?php echo ALAMAT."/".ADMIN_PAGE."/enkoding.html"; ?>">Enkoding</a></li>
			<li <?php if($halaman=='pengguna'){echo' class="current_page_item"';} ?> ><a href="<?php echo ALAMAT."/".ADMIN_PAGE."/pengguna.html"; ?>">Pengguna</a></li>
			<li <?php if($halaman=='pengaturan'){echo' class="current_page_item"';} ?> ><a href="<?php echo ALAMAT."/".ADMIN_PAGE."/pengaturan.html"; ?>">Pengaturan</a></li>
			<li><a href="<?php echo ALAMAT."/".ADMIN_PAGE."/logout.html"; ?>">Keluar</a></li>
<?php } ?>
		</ul>
	</div>
	<div id="menu2">
		<a id="tombol"><img src="<?php echo ALAMAT."/images/petunjuk1.png"; ?>"></a>
	</div>
</div>
<?php if($p==ADMIN_PAGE){?>
<div id="logoa">
	<h1>Halaman Admin</h1>
	<h2>atur aplikasimu disini</h2>
</div>
<?php }else{ ?>
<div id="logo">
	<a href="<?php echo ALAMAT; ?>"><img src="images/title.jpg" title="Aplikasi Validasi Citra Dokumen"></a>
<?php if ($cek->adaAturan()){ ?>
	<form method="POST" action="" enctype="multipart/form-data">
	<h1><input type="file" name="gambar" size="80px">	</h1>
	<h2><input type="submit" value="Periksa" name="dekoding"></h2>
	</form>
<?php }else{ ?>
	<script type="text/javascript">alert('Parameter aplikasi belum ditentukan, silakan hubungi pengurus aplikasi');</script>
<?php } ?>
</div>
<?php } ?>
<!-- end header -->
</div>
<!-- start page -->
<div id="page">
<!-- start content -->
	<div id="content">
		<div class="post">
<?php
	if(file_exists("modul/".$halaman."/".$halaman."UI.php")){
		require "modul/".$halaman."/".$halaman."UI.php";
	}elseif($halaman=='muka'){
?>
			<div id="petunjuk" title="Petunjuk">
				Selamat datang di halaman admin, silakan pilih salah satu menu di atas<br>
				<img src="<?php echo ALAMAT.'/petunjuk/admin1.jpg'; ?>" width="280px">
			</div>
			<h1 class="title">Selamat Datang</h1>
			<div class="entry">
				berhasill...berhasil...horee....
			</div>
<?php
	}
?>
		</div>
	</div>
<!-- end content -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<!-- start footer -->
<div id="footer">
	<div id="footer-wrap">
	<p id="legal">&copy; 2012. Dibikin buat syarat lulus kuliah. Dibikin oleh <a target="_blank" href="http://facebook.com/riyandika">riyandika</a></p>
	</div>
</div>
<!-- end footer -->


</body>
</html>
